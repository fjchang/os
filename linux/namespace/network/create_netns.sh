#!/bin/bash -x

ip link add br0 type bridge
ip link set dev br0 up

for i in 1 2 3; do
  netns="netns${i}"
  veth_out="veth${i}_out"
  veth_in="veth${i}_in"
  veth_in_ip="10.1.1.${i}"

  ip netns add $netns
  ip link add $veth_in type veth peer name $veth_out
  ip link set dev $veth_in netns $netns
  ip netns exec $netns ip addr add ${veth_in_ip}/24 dev $veth_in
  ip netns exec $netns ip link set dev $veth_in up
  ip link set dev $veth_out master br0
  ip link set dev $veth_out up
done
