#!/bin/bash -x

for i in 1 2 3; do
  ip link del veth${i}_out
  ip netns del netns${i}
done

ip link del br0
