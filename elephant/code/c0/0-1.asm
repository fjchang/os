section my_code vstart=0
    jmp 0x7c0:start
    start:       ;标号start只是为了jmp跳到下一条指令

  ;初始化数据段寄存器DS
    mov ax,section.my_data.start
    add ax,0x7c00   ;加0x7c00是因为本程序会被mbr加载到内存0x7c00处
    shr ax,4     ;提前右移4位,因为段基址会被CPU段部件左移4位
    mov ds,ax

  ;初始化栈段寄存器SS
    mov ax,section.my_stack.start
    add ax,0x7c00  ;加0x7c00是因为本程序会被mbr加载到内存0x7c00处
    shr ax,4    ;提前右移4位,因为段基址会被CPU段部件左移4位
    mov ss,ax
    mov sp,stack_top   ;初始化栈指针

  ;此时CS､DS､SS段寄存器已经初始化完成,下面开始正式工作
    push word [var2]   ;变量名var2编译后变成0x4
    jmp $

  ;自定义的数据段
section my_data align=16 vstart=0
    var1 dd 0x1
    var2 dd 0x6

  ;自定义的栈段
section my_stack align=16 vstart=0
    times 128 db 0
stack_top:  ;此处用于栈顶,标号作用域是当前section,
              ;以当前section的vstart为基数

  times 318 db 0

  db 0x55
  db 0xAA
