#!/bin/sh

if [ $# -eq 0 -o $# -gt 2 ]; then
  echo 'Usage $0 <MBR> [<floppy>]'
  echo '  MBR: name of a input file containing exactly 512-byte MBR image'
  echo '  floppy: name of the output file used for 1.44M floppy image'
  exit
fi

MBR=$1
FLP=${2:-a.img}

if [ ! -f "$MBR" ]; then
  echo "File $MBR not found"
  exit
elif [ ! -r "$MBR" ]; then
  echo "File $MBR not readable"
  exit
fi

mbrsize=`du -b --apparent-size "$MBR" | cut -f1`
if [ $mbrsize -ne 512 ]; then
  echo "Size of file $MBR must be exactly 512 bytes"
  exit
fi

if [ -e $FLP ]; then
  echo "File $FLP exists"
  exit
fi

bximage -mode=create -fd=1.44M -q $FLP

dd if=$MBR of=$FLP bs=512 count=1 conv=notrunc

