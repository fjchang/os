#!/bin/sh

./configure --prefix=/usr/local \
            --enable-cpu-level=4 \
            --enable-debugger \
            --disable-debugger-gui \
            --enable-disasm \
            --enable-ne2000 \
            --enable-pci \
            --with-sdl \
            --enable-all-optimizations \
            --with-all-libs \
            ${CONFIGURE_ARGS}
